/* checkmsva: MonkeySphere Validation library test program
 *   Copyright © 2011-2013  Clint Adams

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>

#include "msv/msv.h"

int
main (int argc, char **argv)
{
  int x;
  msv_ctxt_t ctx;
  struct msv_query query;
  struct msv_response *response;

  ctx = msv_ctxt_init (NULL);
  if (!ctx) {
      printf ("Unable to initialize context\n"
              "Make sure MONKEYSPHERE_VALIDATION_AGENT_SOCKET env variable is set and\n"
              "the MSVA validation agent is running\n");
      exit (1);
  }

  x = msv_check_msva (ctx);
  printf ("MSVA check: %s\n", msv_strerror (ctx, x));

  query.context = "https";
  query.peertype = "server";
  query.peername = "www.softwarefreedom.org";
  query.pkctype = "x509pem";
  query.pkcdata = "-----BEGIN CERTIFICATE-----\n"
    "MIIE7DCCA9SgAwIBAgIRAPBGJ9FB7isLF2bGpDQ0SQ4wDQYJKoZIhvcNAQEFBQAw\n"
    "QTELMAkGA1UEBhMCRlIxEjAQBgNVBAoTCUdBTkRJIFNBUzEeMBwGA1UEAxMVR2Fu\n"
    "ZGkgU3RhbmRhcmQgU1NMIENBMB4XDTExMDMyMjAwMDAwMFoXDTEyMDMyMTIzNTk1\n"
    "OVowYjEhMB8GA1UECxMYRG9tYWluIENvbnRyb2wgVmFsaWRhdGVkMRswGQYDVQQL\n"
    "ExJHYW5kaSBTdGFuZGFyZCBTU0wxIDAeBgNVBAMTF3d3dy5zb2Z0d2FyZWZyZWVk\n"
    "b20ub3JnMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApPKJf7HryKWS\n"
    "g9qEuUGQPTXmw3xY1ktwwDH0sgxk2GzDR73f+zrjBgCynGe/jNCWaPQBqSjWu8b4\n"
    "bfd/7rT4E2peisg1ZnKZW0fsAD8ZeFhoX604ZEO7sYLNhQgtU9NuOY3go0aqgUbP\n"
    "2e7bRd5UlgBqLmBJhuOHbq51PpgYueuhHTAgCea9qij3YtS9Cg7aiwEDE3R+Wenw\n"
    "QVzJa3XZmmraEhpIsakU+h48V6iMuI2QjgmVP2KV74xYc8SkHNVPLBHQ/TSZEXtQ\n"
    "+GDN9HRW1Oz+tqKOA8cMEBwaLGzpc0cK7nwkRGZvU+S7BI3D8fbz1zdaF5o9ikcG\n"
    "38Hdh27r1wIDAQABo4IBvDCCAbgwHwYDVR0jBBgwFoAUtqj/oqgv0KbNS7Fo8+dQ\n"
    "EDGneSEwHQYDVR0OBBYEFE7fMGOazT4eO4vaURq8QmNSDkblMA4GA1UdDwEB/wQE\n"
    "AwIFoDAMBgNVHRMBAf8EAjAAMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcD\n"
    "AjBWBgNVHSAETzBNMEsGCysGAQQBsjEBAgIaMDwwOgYIKwYBBQUHAgEWLmh0dHA6\n"
    "Ly93d3cuZ2FuZGkubmV0L2NvbnRyYWN0cy9mci9zc2wvY3BzL3BkZi8wPAYDVR0f\n"
    "BDUwMzAxoC+gLYYraHR0cDovL2NybC5nYW5kaS5uZXQvR2FuZGlTdGFuZGFyZFNT\n"
    "TENBLmNybDBqBggrBgEFBQcBAQReMFwwNwYIKwYBBQUHMAKGK2h0dHA6Ly9jcnQu\n"
    "Z2FuZGkubmV0L0dhbmRpU3RhbmRhcmRTU0xDQS5jcnQwIQYIKwYBBQUHMAGGFWh0\n"
    "dHA6Ly9vY3NwLmdhbmRpLm5ldDA3BgNVHREEMDAughd3d3cuc29mdHdhcmVmcmVl\n"
    "ZG9tLm9yZ4ITc29mdHdhcmVmcmVlZG9tLm9yZzANBgkqhkiG9w0BAQUFAAOCAQEA\n"
    "P1DcKGoP2cfAUGxT6N8BLVKib7wrhsqhy4u5+7sZyBSdo0r6MsyOqw+J68bcx7wZ\n"
    "v7/khnx53Qgh3Xm97QXmua6c89BsQkr4EdWiKyu1bNimXksmtiUlkCiC63vakjrQ\n"
    "IJroa64ATbxEXoJa6HgQNCZHLRAwPecD7eCiwyUX7KZG8dnC5983FbtP+RD647P8\n"
    "Nhv5d9fALco89VqiVHSNSuMIrd83q+6T5ecK+Z9e3GkAqTIbrosV0TXtGO7iuW9G\n"
    "xHMcOGjzfj0LUSqVEVe/D8VzeTEQiNs1MVuRtpQ/dciEL2dkXqjzZiuuhs6TIvDG\n"
    "6wknKthAJdFSXGQkb7kGiw==\n" "-----END CERTIFICATE-----\n";
  x = msv_query_agent (ctx, query, &response);
  if (!response) {
      printf ("MSV query: failure contacting MSV agent\n");
      exit (2);
  }
  printf ("MSV query: %s\nValid: %s\nMessage: %s\n", msv_strerror (ctx, x),
	  response->valid ? "true" : "false", response->message);

  exit (x);
}
