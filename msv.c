/* libmsv: MonkeySphere Validation library
 *   Copyright © 2011-2013  Clint Adams

 * libmsv is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 
 * libmsv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 
 * You should have received a copy of the GNU General Public License
 * along with libmsv; if not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include <jansson.h>

#include "msv/msv.h"

struct msv_ctxt
{
  char *socket_location;
  CURLcode cc;
};

extern const char *
msv_strerror (msv_ctxt_t ctx, int error_code)
{
  if (ctx)
    {
      switch (error_code)
	{
	case LIBMSV_ERROR_SUCCESS:
	  return "No error";
	case LIBMSV_ERROR_INVALID:
	  return "Agent reports invalid";
	case LIBMSV_ERROR_NOENVVAR:
	  return "Environment variable not set";
	case LIBMSV_ERROR_CURLINIT_FAILED:
	  return "curl_easy_init() failed!";
	case LIBMSV_ERROR_INCOMPATIBLE_AGENT:
	  return "Validation agent is incompatible";
	case LIBMSV_ERROR_BADARG:
	  return "Bad argument(s)";
	case LIBMSV_ERROR_CURLCODE:
	  return curl_easy_strerror (ctx->cc);
	default:
	  return "Unknown error";
	}
    }
  else
    return "Unknown context; misuse of msv_strerror";
}

static size_t
write_function (void *ptr, size_t size, size_t nmemb, void *userdata)
{
  char *ud;

  ud = malloc (size * nmemb + 1);
  if (!ud)
      return 0;
  memcpy (ud, ptr, size * nmemb);

  ud[size * nmemb] = '\0';

  *((char **) userdata) = (void *) ud;

  return size * nmemb;
}

extern msv_ctxt_t
msv_ctxt_init (const char *url)
{

  msv_ctxt_t ctx;
  char *env;

  ctx = malloc (sizeof (struct msv_ctxt));
  if (!ctx)
    return NULL;

  if (url)
    ctx->socket_location = strdup (url);
  else if ((env = getenv ("MONKEYSPHERE_VALIDATION_AGENT_SOCKET")))
    ctx->socket_location = strdup (env);
  else
    {
      free (ctx);
      return NULL;
    }

  if (!ctx->socket_location)
    {
      free (ctx);
      return NULL;
    }

  return ctx;
}

extern void
msv_ctxt_destroy (msv_ctxt_t ctx)
{
  if (ctx)
    {
      if (ctx->socket_location)
	free (ctx->socket_location);
      free (ctx);
    }
}

extern int
msv_check_msva (msv_ctxt_t ctx)
{
  CURL *c;
  char *buf;
  json_t *j, *k;
  json_error_t je;
  void *iter;
  int protoversion, available, rv;

  if (!ctx)
    return LIBMSV_ERROR_BADARG;
  if (ctx->socket_location == NULL)
    return LIBMSV_ERROR_NOENVVAR;

  c = curl_easy_init ();
  if (c == NULL)
    return LIBMSV_ERROR_CURLINIT_FAILED;

  ctx->cc = curl_easy_setopt (c, CURLOPT_URL, ctx->socket_location);
  if (ctx->cc)
    {
      curl_easy_cleanup (c);
      return LIBMSV_ERROR_CURLCODE;
    }
  ctx->cc = curl_easy_setopt (c, CURLOPT_WRITEFUNCTION, write_function);
  if (ctx->cc)
    {
      curl_easy_cleanup (c);
      return LIBMSV_ERROR_CURLCODE;
    }
  ctx->cc = curl_easy_setopt (c, CURLOPT_WRITEDATA, &buf);
  if (ctx->cc)
    {
      curl_easy_cleanup (c);
      return LIBMSV_ERROR_CURLCODE;
    }

  ctx->cc = curl_easy_perform (c);
  if (ctx->cc)
    {
      curl_easy_cleanup (c);
      return LIBMSV_ERROR_CURLCODE;
    }

  if ((j = json_loads (buf, 0, &je)))
    {
      free (buf);

      iter = json_object_iter_at (j, "protoversion");
      if (iter)
	{
	  k = json_object_iter_value (iter);

	  json_unpack (k, "i", &protoversion);

	  iter = json_object_iter_at (j, "available");
	  if (iter)
	    {
	      k = json_object_iter_value (iter);

	      json_unpack (k, "b", &available);

	      rv = 0;
	    }
	  else
	    {
	      rv = LIBMSV_ERROR_UNEXPECTED_RESPONSE;
	    }
	}
      else
	{
	  rv = LIBMSV_ERROR_UNEXPECTED_RESPONSE;
	}
      json_decref (j);

    }
  else
    {
      rv = LIBMSV_ERROR_UNEXPECTED_RESPONSE;
    }
  curl_easy_cleanup (c);
  if (rv)
    {
      return rv;
    }
  else
    {
      if (protoversion == 1 && available == 1)
	return LIBMSV_ERROR_SUCCESS;
      else
	return LIBMSV_ERROR_INCOMPATIBLE_AGENT;
    }
}

extern int
msv_query_agent (msv_ctxt_t ctx, struct msv_query q,
		 struct msv_response **response_ptr)
{

  int oldurllen, rv = 0;
  CURL *c;
  CURLcode cc;
  char *buf, *req = NULL, *reviewurl;
  json_t *j, *k, *jreq, *pkc, *peer, *j_pkcdata, *j_pkctype, *j_peername,
    *j_peertype, *j_context;
  json_error_t je;
  void *iter;
  struct curl_slist *slist = NULL;
  struct msv_response *response;

  if (!response_ptr)
    return LIBMSV_ERROR_BADARG;

  *response_ptr = NULL;

  if (!ctx)
    return LIBMSV_ERROR_BADARG;
  if (ctx->socket_location == NULL)
    return LIBMSV_ERROR_NOENVVAR;


  if (!(q.context && q.peertype && q.peername && q.pkctype && q.pkcdata))
    return LIBMSV_ERROR_BADARG;

  jreq = json_object ();
  pkc = json_object ();
  peer = json_object ();

  if ((j_pkcdata = json_string (q.pkcdata)) &&
      (j_pkctype = json_string (q.pkctype)) &&
      (j_peername = json_string (q.peername)) &&
      (j_peertype = json_string (q.peertype)) &&
      (j_context = json_string (q.context)) &&
      (json_object_set (pkc, "data", j_pkcdata) == 0) &&
      (json_object_set (pkc, "type", j_pkctype) == 0) &&
      (json_object_set (peer, "name", j_peername) == 0) &&
      (json_object_set (peer, "type", j_peertype) == 0) &&
      (json_object_set (jreq, "pkc", pkc) == 0) &&
      (json_object_set (jreq, "context", j_context) == 0) &&
      (json_object_set (jreq, "peer", peer) == 0))
    {
      req = json_dumps (jreq, JSON_PRESERVE_ORDER | JSON_COMPACT);
    }
  else
    {
      rv = LIBMSV_ERROR_BADARG;
    }

  if (j_pkcdata)
    json_decref (j_pkcdata);
  if (j_pkctype)
    json_decref (j_pkctype);
  if (j_peername)
    json_decref (j_peername);
  if (j_peertype)
    json_decref (j_peertype);
  if (j_context)
    json_decref (j_context);
  if (peer)
    json_decref (peer);
  if (pkc)
    json_decref (pkc);
  if (jreq)
    json_decref (jreq);

  if (rv)
    return rv;

  c = curl_easy_init ();
  if (c == NULL)
    {
      free (req);
      return LIBMSV_ERROR_CURLINIT_FAILED;
    }

  oldurllen = strlen (ctx->socket_location);
  reviewurl = malloc (oldurllen + 12);
  if (!reviewurl)
    {
        curl_easy_cleanup (c);
        free (req);
        return LIBMSV_ERROR_NOMEM;
    }
  sprintf (reviewurl, "%s%s", ctx->socket_location, "/reviewcert");

  cc = curl_easy_setopt (c, CURLOPT_URL, reviewurl);
  free (reviewurl);
  if (cc)
    {
      curl_easy_cleanup (c);
      free (req);
      return LIBMSV_ERROR_CURLCODE;
    }
  cc = curl_easy_setopt (c, CURLOPT_WRITEFUNCTION, write_function);
  if (cc)
    {
      curl_easy_cleanup (c);
      free (req);
      return LIBMSV_ERROR_CURLCODE;
    }
  cc = curl_easy_setopt (c, CURLOPT_WRITEDATA, &buf);
  if (cc)
    {
      curl_easy_cleanup (c);
      free (req);
      return LIBMSV_ERROR_CURLCODE;
    }
  cc = curl_easy_setopt (c, CURLOPT_POST, 1);
  if (cc)
    {
      curl_easy_cleanup (c);
      free (req);
      return LIBMSV_ERROR_CURLCODE;
    }

  slist = curl_slist_append (slist, "Content-Type: application/json");
  slist = curl_slist_append (slist, "Connection: close");
  slist = curl_slist_append (slist, "Accept: application/json");
  slist = curl_slist_append (slist, "Expect:");
  cc = curl_easy_setopt (c, CURLOPT_HTTPHEADER, slist);
  if (cc)
    {
      curl_easy_cleanup (c);
      free (req);
      curl_slist_free_all (slist);
      return LIBMSV_ERROR_CURLCODE;
    }

  cc = curl_easy_setopt (c, CURLOPT_POSTFIELDS, (void *) req);
  if (cc)
    {
      curl_easy_cleanup (c);
      free (req);
      curl_slist_free_all (slist);
      return LIBMSV_ERROR_CURLCODE;
    }

  cc = curl_easy_perform (c);
  if (cc)
    {
      curl_easy_cleanup (c);
      free (req);
      curl_slist_free_all (slist);
      return LIBMSV_ERROR_CURLCODE;
    }

  if (!(response = malloc (sizeof (struct msv_response))))
    {
      curl_easy_cleanup (c);
      free (req);
      curl_slist_free_all (slist);
      return LIBMSV_ERROR_NOMEM;
    }
  response->message = NULL;
  *response_ptr = response;

  j = json_loads (buf, 0, &je);
  free (buf);

  iter = json_object_iter_at (j, "valid");
  k = json_object_iter_value (iter);

  json_unpack (k, "b", &response->valid);

  iter = json_object_iter_at (j, "message");
  k = json_object_iter_value (iter);

  json_unpack (k, "s", &response->message);
  response->message = strdup (response->message);
  json_decref (j);

  curl_slist_free_all (slist);

  curl_easy_cleanup (c);
  free (req);

  if (!response->message)
    {
      /* strdup returned NULL, which suggests a memory allocation
         failure */
      msv_response_destroy (response);
      *response_ptr = NULL;
      return LIBMSV_ERROR_NOMEM;
    }

  return !(response->valid);
}

extern void
msv_response_destroy (struct msv_response *response)
{
  if (response)
    {
      if (response->message)
	free (response->message);
      free (response);
    }
}
