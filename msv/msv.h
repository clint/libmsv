#ifndef _MSV_H
#define _MSV_H 1

#include <unistd.h>

/** @file msv/msv.h
 *  @brief main public libmsv header file
 */

/**
 * @def LIBMSV_ERROR_SUCCESS
 * @brief Success or absence of error
 */
#define LIBMSV_ERROR_SUCCESS 0
/**
 * @def LIBMSV_ERROR_INVALID
 * @brief Use of certificate could not be determined valid or was determined invalid
 */
#define LIBMSV_ERROR_INVALID 1
/**
 * @def LIBMSV_ERROR_NOENVVAR
 * @brief Environment variable was needed but was not set
 */
#define LIBMSV_ERROR_NOENVVAR 2
/**
 * @def LIBMSV_ERROR_CURLINIT_FAILED
 * @brief curl_easy_init() failed for unknown reason
 */
#define LIBMSV_ERROR_CURLINIT_FAILED 3
/**
 * @def LIBMSV_ERROR_CURLCODE
 * @brief an unspecified curl function failed
 *
 * The MSV context retains the curl error code for the last operation
 * run within that context
 */
#define LIBMSV_ERROR_CURLCODE 4
/**
 * @def LIBMSV_ERROR_INCOMPATIBLE_AGENT
 * @brief MSVA was perceived as incompatible
 */
#define LIBMSV_ERROR_INCOMPATIBLE_AGENT 5
/**
 * @def LIBMSV_ERROR_BADARG
 * @brief An argument to the function was invalid
 */
#define LIBMSV_ERROR_BADARG 6

/**
 * @def LIBMSV_ERROR_UNEXPECTED_RESPONSE
 * @brief The response from MSVA was unexpected
 */
#define LIBMSV_ERROR_UNEXPECTED_RESPONSE 7

/**
 * @def LIBMSV_ERROR_NOMEM
 * @brief necessary memory allocation failed
 */
#define LIBMSV_ERROR_NOMEM 8

/**
 * @typedef msv_ctxt_t
 * @brief opaque MSVA context
 */
struct msv_ctxt;
typedef struct msv_ctxt *msv_ctxt_t;

/**
 * @struct msv_query
 * @brief a query to the MSVA
 */
struct msv_query
{
  const char *context; /**< certificate use context, such as "https", "ssh", or "ike" */
  const char *peertype;	/**< type of peer on remote side, such as "client", "server", or "peer" */
  const char *peername;	/**< name of peer, such as "web.monkeysphere.info" */
  const char *pkctype; /**< format of public key carrier data, such as "x509der", "x509pem", or "opensshpubkey" */
  const char *pkcdata; /**< public key carrier data encoded in the format specified by \a pkctype */
};

#ifdef HAVE_VAR_ATTRIBUTE_DEPRECATED
# define MSV_DEPRECATED(X) X __attribute__((deprecated))
#elif __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 5)
# define MSV_DEPRECATED(X) X __attribute__((deprecated))
#elif __GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 1)
# define MSV_DEPRECATED(X) X __attribute__((__deprecated__))
#else
# define MSV_DEPRECATED(X) X
#endif

/**
 * @struct msv_response
 * @brief a response from the MSVA
 */
struct msv_response
{
  int valid; /**< 0 for invalid, 1 for valid */
  char *message; /**< text response to be displayed to the user */
  size_t MSV_DEPRECATED(sz); /**< length of the buffer allocated to \a message */
};

/**
 * @fn msv_ctxt_t msv_ctxt_init (const char *url)
 * @brief Return an MSV context
 * @param url URL for MSVA or NULL to use MONKEYSPHERE_VALIDATION_AGENT_SOCKET environment variable
 * @return MSV context
 */
extern msv_ctxt_t msv_ctxt_init (const char *url);

/**
 * @fn void msv_ctxt_destroy (msv_ctxt_t ctx)
 * @brief Destroy an MSV context
 * @param ctx the MSV context to destroy
 */
extern void msv_ctxt_destroy (msv_ctxt_t ctx);

/**
 * @fn const char *msv_strerror (msv_ctxt_t ctx, int error_code)
 * @brief Return string describing libmsv error code
 * @param ctx the relevant MSV context
 * @param error_code return value of libmsv function
 * @return string describing error
 */
extern const char *msv_strerror (msv_ctxt_t ctx, int error_code);

/**
 * @fn int msv_check_msva (msv_ctxt_t ctx)
 * @brief Check suitability of Monkeysphere Validation Agent
 * @param ctx the MSV context
 * @return 0 for success, or libmsv error code
 */
extern int msv_check_msva (msv_ctxt_t ctx);

/**
 * @fn int msv_query_agent (msv_ctxt_t ctx, struct msv_query q, struct msv_response **response_ptr)
 * @brief Query validation agent for certificate validity
 * @param ctx the MSV context
 * @param q msv_query struct representing the MSVA query
 * @param response_ptr pointer to pointer to msv_response struct that the caller is responsible for destroying
 *        via \a msv_response_destroy
 * @return 0 for valid use of certificate, 1 for invalid use of certificate, or libmsv error code
 */
extern int msv_query_agent (msv_ctxt_t ctx, struct msv_query q,
			    struct msv_response **response_ptr);

/**
 * @fn void msv_response_destroy (struct msv_response *response)
 * @brief Destroy an MSV response
 * @param response the MSV response to destroy
 */
extern void msv_response_destroy (struct msv_response *response);

#endif /* _MSV_H */
